<?php

namespace App\Form;

use App\Entity\Lieu;
use App\Entity\Site;
use App\Entity\Sortie;
use App\Entity\Status;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateSortieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre',null)
            ->add('description',null)
            ->add('dateLimiteInscription',DateType::class, [
                'widget' => 'single_text',
                'input'  => 'datetime_immutable'
            ])
            ->add('dateSortie', DateType::class, [
                'widget' => 'single_text',
                'input'  => 'datetime_immutable'
            ])
            ->add('site', EntityType::class, [
                'label'=>'Site',
                'class'=>Site::class,
                'choice_label'=>function($site){
                    return $site->getNom();
                }
            ])
            ->add('nbMaxParticipants',null)
            ->add('lieu', EntityType::class, [
                'label'=>'Lieu',
                'class'=>Lieu::class,
                'choice_label'=>function($lieu){
                    return $lieu->getNom();
                }
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer',
                'attr' => ['class' => 'pure-button-create pure-button pure-button-primary']
            ])
            ->add('publish', SubmitType::class, [
                'label' => 'Publier',
                'attr' => ['class' => 'pure-button-create pure-button pure-button-primary']
             ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Sortie::class,
        ]);
    }
}
