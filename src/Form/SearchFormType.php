<?php

namespace App\Form;

use App\Utils\SearchSortie;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Site;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomSite', EntityType::class, [
                'label'=>'Site',
                'class'=>Site::class,
                'choice_label'=>function($site){
                    return $site->getNom();
                }
            ])
            ->add('nomSortie', TextType::class, [
                'label' => "Nom sortie",
                'required' => false
            ])
            ->add('date', DateType::class, [
                'label' => "Date début sortie",        
                'required' => false,
                'widget' => 'single_text'
            ])
            ->add('dateFin', DateType::class, [
                'label' => "Date fin sortie",
                'required' => false,
                'widget' => 'single_text'
            ])
        ;
    }

    //à redéfinir pour imposer un nom pour le form
    public function getBlockPrefix()
    {
        return "searchForm";
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SearchSortie::class,
            'csrf_protection'   => false,
        ]);
    }
}
