<?php


namespace App\Utils;


use Symfony\Component\Validator\Constraints as Assert;

class SearchSortie
{
    /**
     * @Assert\Type("string")
    */
    private $nomSortie;

    private $nomSite;

    private $date;

    private $dateFin;

    public function getNomSortie()
    {
        return $this->nomSortie;
    }


    public function setNomSortie($nomSortie): void
    {
        $this->nomSortie = $nomSortie;
    }

    public function getNomSite()
    {
        return $this->nomSite;
    }

    public function setNomSite($nomSite): void
    {
        $this->nomSite = $nomSite;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date): void
    {
        $this->date = $date;
    }

    public function getDateFin()
    {
        return $this->dateFin;
    }

    public function setDateFin($dateFin): void
    {
        $this->dateFin = $dateFin;
    }

}