<?php

namespace App\DataFixtures;

use App\Entity\Utilisateur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class AppFixtures extends Fixture
{
    private $entityManager;
    private $generator;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->generator = Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager): void
    {
        $this->addUsers();

        $manager->flush();
    }

    private function addUsers()
    {

        for ($i = 0; $i < 5; $i++) {

            $user = new Utilisateur();
            $user->setRoles(["ROLE_USER"])
                ->setPrenom($this->generator->firstName)
                ->setNom($this->generator->lastName)
                ->setEmail($this->generator->email)
                ->setPassword($this->generator->password)
                // ->setPassword($this->encoder->encodePassword($user, '123'))
                ->setPseudo($this->generator->word)
                ->setTelephone($this->generator->phoneNumber);
                

            $this->entityManager->persist($user);
        }
    }
}
