<?php

namespace App\Controller;

use App\Entity\Sortie;
use App\Entity\Status;
use App\Form\AnnulationSortieType;
use App\Form\CreateSortieType;
use App\Form\SearchFormType;
use App\Repository\InscriptionRepository;
use App\Repository\LieuRepository;
use App\Repository\SortieRepository;
use App\Repository\StatusRepository;
use App\Repository\VilleRepository;
use App\Utils\SearchSortie;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SortieController extends AbstractController
{
    const SORTIE_LIMITE = 10;
        /**
     * @Route("/sortie/list/{page}", name="sortie_list",  requirements={"page" = "\d+"})
     * Injection de dépendance d'une instance de la class SortieRepository
     */
    public function list(SortieRepository $sortieRepository, int $page = 1, Request $request): Response
    {

        $searchSortie = new SearchSortie();
        $sortieForm = $this->createForm(SearchFormType::class, $searchSortie, [
            //définis la méthode GET pour ce formulaire
            'method' => 'GET',
            //régénère l'url en cas de nouvelle soumission
            'action' => $this->generateUrl('sortie_list')
        ]);
        $sortieForm->handleRequest($request);




        $nbSortieMax = $sortieRepository->count([]);
        $maxPage = ceil($nbSortieMax / 50);

        //utilisation de la requête personnalisée
        if (($page > 0 && $page <= $maxPage) || $nbSortieMax == 0) {
            $sorties = $sortieRepository->findSorties($searchSortie, $page);

        } else {
            //si pas sur une page correct, on lève renvoie un erreur 404
            throw $this->createNotFoundException("Oops ! This page does not exist !");
        }

        $maxPage = ceil($sorties['maxSorties'] / self::SORTIE_LIMITE);

        return $this->render('sortie/list.html.twig', [
            'sorties' => $sorties['sorties'],
            'sortieForm'=> $sortieForm->createView(),
            'maxSorties'=>$sorties['maxSorties'],
            'currentPage' => $page,
            'maxPage' => $maxPage
        ]);
    }

    
    /**
     * @Route("/sorties/create/", name="sortie_create")
     */
    public function create(Request $request, EntityManagerInterface $entityManager, VilleRepository $villeRepository, StatusRepository $statusRepository): Response {
        $sortie = new Sortie();
        $createSortieForm = $this->createForm(CreateSortieType::class, $sortie, ['attr' => ['class' => 'pure-form pure-form-aligned']]
    );
        $createSortieForm->handleRequest($request);
        $villes=$villeRepository->findAll();

        if($createSortieForm->isSubmitted() && $createSortieForm->isValid()){

            if($createSortieForm->get('save')->isClicked()){
                $status = $statusRepository->find(2);
                $sortie= $sortie->setStatus($status);
            }

            if($createSortieForm->get('publish')->isClicked()){
                $status = $statusRepository->find(1);
                $sortie= $sortie->setStatus($status);         
            }

            $entityManager->persist($sortie);
            $entityManager->flush();
            return $this->redirectToRoute('sortie_detail', ['sortieId' => $sortie->getId()]);    

         }

         return $this->render('sortie/create.html.twig', [
             "createSortie" => $createSortieForm->createView(),
             "villes"=>$villes
         ]);
    }

    /**
     * @Route("/get-lieux/{villeId}", name="app_get_lieux")
     */
    public function getLieux(LieuRepository $repository, $villeId): Response {
        $lieux = $repository->findBy(['ville'=>$villeId]);

        $json = '[';
        foreach($lieux as $lieu){
            $json .='{"nomLieu":"'.$lieu->getNom().'","id":"'.$lieu->getId().'"},';
        }
        $json = substr($json,0,-1);
        $json .= ']';
        return $this->json($json);
    }

     /**
     * @Route("/sorties/{sortieId}", name="sortie_detail")
     */
    public function detailSortie(InscriptionRepository $inscriptionRepository, SortieRepository $repository, $sortieId, Request $request, EntityManagerInterface $entityManager): Response {

        $user = $this->getUser();
        // On vérifie si l'utilisateur est déjà inscrit
        $inscription = $inscriptionRepository->findOneBy(["sortie" => $sortieId, "utilisateur" => $user->getId()]);
        //On récupére le nombre d'inscrits à la sortie
        $nbInscrits = $inscriptionRepository->countInscription($sortieId);

        $sortie = $repository->findOneBy(["id" => $sortieId]);
        $createSortieForm = $this->createForm(AnnulationSortieType::class, $sortie);
        $createSortieForm->handleRequest($request);
        if(!$sortie) {
            throw $this->createNotFoundException('This sortie doesn\'t exist !');
        }

         if($createSortieForm->isSubmitted() && $createSortieForm->isValid()){

            if($createSortieForm->get('annuler')->isClicked()) {
                    $this->annulerSortie($createSortieForm, $sortie); 
            }
        }

        return $this->render('sortie/detail.html.twig', [
            'inscription' => $inscription,
            'sortie' => $sortie,
            'nbInscrits' => $nbInscrits,
            'annulationSortie'=>$createSortieForm->createView()
        ]);

    }
    /**
     * @Route("/desist/{id}", name="app_desist")
     */
    public function desist(InscriptionRepository $inscriptionRepository,
                            EntityManagerInterface $entityManager,  $id = 0): Response {
               
        $user = $this->getUser();
        $inscription = $inscriptionRepository->findOneBy(["sortie" => $id, "utilisateur" => $user->getId()]);

        $now = new DateTime('NOW');

        if ($inscription->getSortie()->getDateSortie() > $now) {
            $entityManager->remove($inscription);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_main');
    }


    public function annulerSortie( $createSortieForm, Sortie $sortie) {
        $em = $this->getDoctrine()->getManager();
        $test = $em->getRepository(Status::class);
        $status = $test->find(3);
        $sortie = $createSortieForm->getData();
        $sortie= $sortie->setStatus($status); 
        $em->persist($sortie);
        $em->flush();

    }

    /**
     * @Route("/sorties/publish/{id}", name="sortie_publish", requirements={"id" = "\d+"}))
     */
    public function publierSortie(int $id, SortieRepository $repository) {
        $em = $this->getDoctrine()->getManager();
        $sortie = $repository->find($id);
        $test = $em->getRepository(Status::class);
        $status = $test->find(1);
        $sortie= $sortie->setStatus($status);  
        $em->persist($sortie);
        $em->flush();

        return $this->redirectToRoute('sortie_detail', ['sortieId' => $sortie->getId()]);    
    }
}
