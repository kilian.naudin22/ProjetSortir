<?php

namespace App\Controller;

use App\Entity\Inscription;
use App\Form\InscriptionType;
use App\Repository\InscriptionRepository;
use App\Repository\SortieRepository;
use App\Repository\StatusRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InscriptionController extends AbstractController
{
    /**
     * @Route("/inscription", name="inscription")
     */
    public function index(): Response
    {
        return $this->render('inscription/index.html.twig', [
            'controller_name' => 'InscriptionController',
        ]);
    }

    /**
     * @Route("/inscription/create/{id}", name="inscription_create", requirements={"id" = "\d+"}))
     */
    public function create(int $id, EntityManagerInterface $entityManager, SortieRepository $sortieRepository, InscriptionRepository $repository): Response{

        $inscription = $repository->findOneBy(["sortie" => $id, "utilisateur" => $this->getUser()->getId()]);
        
        if(!$inscription){

            $inscription = new Inscription();
            $sortie = $sortieRepository->find($id);
            $inscription= $inscription->setStatus('inscrit');
            $inscription = $inscription->setDateInscription(new DateTime('NOW'));
            $inscription = $inscription->setUtilisateur( $this->getUser());
            $inscription = $inscription->setSortie($sortie);
            $entityManager->persist($inscription);
            $entityManager->flush();
        }
            return $this->redirectToRoute('sortie_detail', ['sortieId' => $inscription->getSortie()->getId()]);    
    }
}
