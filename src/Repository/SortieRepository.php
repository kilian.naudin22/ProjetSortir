<?php

namespace App\Repository;

use App\Entity\Sortie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Sortie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sortie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sortie[]    findAll()
 * @method Sortie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SortieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sortie::class);
    }

    // /**
    //  * @return Sortie[] Returns an array of Sortie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findSorties($sortie, $page) {
        $limit = 10;
        $offset = ($page - 1) * $limit;
        $qb = $this->createQueryBuilder('s');
        $qb
           ->addOrderBy('s.dateSortie', 'DESC')
        ;
        dump($sortie);
        if($sortie->getNomSite())
        {
            $qb->andWhere('s.site = :nomSite');
            $qb->setParameter('nomSite', $sortie->getNomSite());
        }

        if($sortie->getNomSortie()) {
            $qb->andWhere('s.titre LIKE :nomSortie');
            $qb->setParameter('nomSortie', '%'.$sortie->getNomSortie().'%');
        }

        if($sortie->getdate() && $sortie->getDateFin()) {
            $qb->andWhere('s.dateSortie >= :dateSortie');
            $qb->andWhere('s.dateSortie <= :dateFinSortie');
            $qb->andWhere('s.dateSortie >= :actualdate');
            $qb->setParameter('dateSortie', $sortie->getDate());
            $qb->setParameter('dateFinSortie', $sortie->getDateFin());
            $qb->setParameter('actualdate', new \DateTime());

        }

        $qb->select('COUNT(s)');
        $countQuery = $qb->getQuery();
        $maxSorties = $countQuery->getSingleScalarResult();

        $qb->select('s');
        $query = $qb->getQuery('s');
        $query->setFirstResult($offset);
        $query->setMaxResults($limit);

        $paginator = new Paginator($query);
        return [
            'sorties' => $query->getResult(),
            'maxSorties' => $maxSorties
        ];
    }
    


    

    /*
    public function findOneBySomeField($value): ?Sortie
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
